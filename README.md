# download_packages

Script to download R packages locally (including dependencies). Clone the
repository, and run the `Download Packages.R` file to download the dependencies
locally.